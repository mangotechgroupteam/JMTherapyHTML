;(function(mangoFunction){
  if(!$){console.log('jQuery not installed !'); return false;}
  function popUpFrame(el){
    this.init(el);
  }
  popUpFrame.prototype={
    init(el){
      this.$popUpFrame = $(el);
      this.$frameContent=this.$popUpFrame.find('.frameContent');
      console.log(this.$popUpFrame);
      this.setBtn();
    },
    setBtn(){
      this.$popUpFrame.find('[close-btn]').click(()=>{this.closeFrame();
      })
    },
    openFrame(src,type){
      this.$popUpFrame.addClass('active')

      if(type=='img'){
        var $tempImg = $('<img/>');
        $tempImg.addClass('enlargeImg')
        $tempImg.prop('src',src);
        this.$frameContent.html($tempImg);
      }
      else if(type=='video'){
        var $tempVideo = $('<video/>');
        $tempVideo.addClass('enlargeVideo');
        $tempVideo.prop('autoplay',"autoplay");
        var $tempVideoSrc = $('<source/>');
        $tempVideoSrc.prop('src',src);
        this.$frameContent.html($tempVideo.append($tempVideoSrc));
      }
    },
    closeFrame(){
      this.$popUpFrame.removeClass('active')
    } 
  }
  
  function initMap() {
  var jmLoc= {lat:-37.766, lng: 145.0919};
  var jmIcon = "img/Icon_address.png";
  var map = new google.maps.Map(document.getElementsByClassName('homeMap')[0], {
    zoom: 18,
    center: jmLoc
  });
  var marker = new google.maps.Marker({
    position: jmLoc,
    icon:jmIcon,
    map: map
  });
  }
  mangoFunction.initHome = function(){
    $(window).on("scroll",()=>{
      var $bannerH = $('.homeAboutUs').offset().top-100;
      var $scrollTop = $(window).scrollTop();

      if($scrollTop>$bannerH){
        $('.headerWrapper').css("background","#5E423C");
      }
      else{
        $('.headerWrapper').css("background","transparent");
      }
    })
  }
  mangoFunction.initMap = function(){initMap()};
  mangoFunction.popUpFrame=function(el='#popUpFrame'){return new popUpFrame(el);}
  window.mangoFunction = mangoFunction;
})(window.mangoFunction||{});






