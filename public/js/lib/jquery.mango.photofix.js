;(function(mangoFunction){
	if(!$){console.log('jQuery not installed !'); return false;}
	function Photofix(el='.photofix'){
		this.init(el);
	}
	Photofix.prototype={
		init(el){
			this.$photofix = $(el);
			this.setPhotofix();
			$(window).on('resize',()=>{this.setPhotofix();});
		},
		update(){
			this.$photofix = $('.photofix');
		},
		setPhotofix(){
			this.$photofix.each((index,item)=>{
				var $item = $(item);
				var $img = $item.find('img').eq(0);
				var itemRatio = $item.width()/$item.height();
				var imgRatio = $img.width()/$img.height();
				if(itemRatio<imgRatio){
					$img.removeClass('portrait');
					$img.addClass('landscape');
				}else{
					$img.removeClass('landscape');
					$img.addClass('portrait');
				}
			})
		}
	}
	mangoFunction.SetPhotofix=function(){
		return new Photofix();
	}
	window.mangoFunction = mangoFunction;
})(window.mangoFunction||{})
